include /etc/nginx/main.d/*.conf;

worker_processes  auto;

error_log  /var/log/nginx/error.log warn;
pid        /var/run/nginx.pid;

events {
    worker_connections  1024;
}


http {
    include       /etc/nginx/mime.types;
    default_type  application/octet-stream;

    log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                      '$status $body_bytes_sent "$http_referer" '
                      '"$http_user_agent" "$http_x_forwarded_for"';

    access_log  /var/log/nginx/access.log  main;

    open_file_cache max=200000 inactive=20s;
    open_file_cache_valid 30s;
    open_file_cache_min_uses 2;
    open_file_cache_errors on;

    access_log off;

    sendfile        on;
    #tcp_nopush     on;

    keepalive_timeout  65;

    # security, reveal less information about ourselves
    server_tokens off; # disables emitting nginx version in error messages and in the “Server” response header field

    quic_gso on;
    quic_retry on;

    http2_push_preload on;

    # enable response compression
    gzip on;
    gzip_http_version 1.1;
    gzip_vary on;
    gzip_comp_level 6;
    gzip_proxied any;
    gzip_types application/atom+xml application/javascript application/json application/rss+xml application/vnd.ms-fontobject application/x-font-ttf application/x-web-app-manifest+json application/xhtml+xml application/xml font/opentype image/svg+xml image/x-icon text/css text/plain text/x-component;
    gzip_buffers 16 8k;
    gzip_disable msie6;

    brotli on;
    brotli_static on;
    brotli_types image/x-icon application/x-javascript text/javascript application/octet-stream image/gif text/css application/json application/javascript image/png image/webp audio/mpeg;  
    brotli_comp_level 8;

    add_header alt-svc 'h2=":443"; ma=86400, h3-29=":443"; ma=86400, h3=":443"; ma=86400' always;
    add_header x-frame-options "deny";
    add_header Strict-Transport-Security "max-age=31536000" always;

    include /etc/nginx/conf.d/*.conf;
}